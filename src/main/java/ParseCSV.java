import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by joy on 7/24/17.
 */

//for version 2.3 using open-csv
public class ParseCSV {

    public void parseData() throws IOException {

        CSVReader reader = new CSVReader(new FileReader("/home/joy/IdeaProjects/convertToCSV/output.csv"), ',');

        List<Student> student = new ArrayList<Student>();

        List<String[]> records = reader.readAll();

        Iterator<String[]> iterator = records.iterator();

        //skip header row
        iterator.next();

        while(iterator.hasNext()){
            String[] record = iterator.next();

            Student stu = new Student();
            stu.setName(record[0]);
            stu.setAge(record[1]);

            System.out.println(stu);
        }

        reader.close();

    }

    //for version 1.3 using commons-csv
    public void parseCSV() throws IOException {

        //Reader in = new FileReader("/home/joy/IdeaProjects/convertToCSV/output.csv");
        //Create the CSVFormat object
        CSVFormat format = CSVFormat.EXCEL.withHeader("Name","Age");


        //initialize the CSVParser object
        CSVParser parser = new CSVParser(new FileReader("/home/joy/IdeaProjects/convertToCSV/output.csv"), format);

        //creating arraylist to store the csv info
        List<Student> student1 = new ArrayList<Student>();

        //iterating through each record of csv file
        for(CSVRecord record : parser){

            Student stu1 = new Student();

            stu1.setName(record.get("Name"));
            stu1.setAge(record.get("Age"));

            student1.add(stu1);
        }
        //close the parser
        parser.close();

        System.out.println("***");
        //printing the records of csv file in a list
        System.out.println(student1);

    }
}
