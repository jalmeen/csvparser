import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;
import java.io.*;
import java.util.Locale;

/**
 * Created by joy on 7/24/17.
 */
public class HelperJexcel {

    private File csvFile;
    private String excelFilePath;
    private BufferedWriter bw;
    private String fileName;

    public void convert() throws IOException, BiffException {

        csvFile = new File("content.csv");
        OutputStream os = new FileOutputStream(csvFile);
        String encoding = "UTF8";
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(os,encoding);
        bw = new BufferedWriter(outputStreamWriter);

//        int version = 1;
//        while (csvFile.exists())
//        {
//            String newFilename = fileName + version+"."+"csv";
//            csvFile = new File("path" + newFilename);
//            version++;
//        }
//        csvFile.createNewFile();
    }

    public void excelImport() throws IOException, BiffException {
        excelFilePath = "/home/joy/Desktop/text.xls";
        WorkbookSettings workbookSettings = new WorkbookSettings();
        workbookSettings.setLocale(new Locale("en", "EN"));
        Workbook w = Workbook.getWorkbook(new File(excelFilePath), workbookSettings);


        // Gets the sheets from workbook
        for (int sheet = 0; sheet < w.getNumberOfSheets(); sheet++) {
            Sheet s = w.getSheet(sheet);
            bw.write(s.getName());
            bw.newLine();

            Cell[] row = null;

            // Gets the cells from sheet
            for (int i = 0; i < s.getRows(); i++) {
                row = s.getRow(i);

                if (row.length > 0) {
                    bw.write(row[0].getContents());
                    for (int j = 1; j < row.length; j++) {
                        bw.write(',');
                        bw.write(row[j].getContents());
                    }
                }
                bw.newLine();
            }
        }

        bw.flush();
        bw.close();
    }
}
