import jxl.read.biff.BiffException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.IOException;

/**
 * Created by joy on 7/24/17.
 */
public class Main {
    public static void main(String[] args) throws IOException, BiffException, InvalidFormatException {
        System.out.println("Hello World");

        HelperJexcel helperJexcel = new HelperJexcel();

        helperJexcel.convert();
        helperJexcel.excelImport();


        HelperPOI helperPOI = new HelperPOI();
        helperPOI.convertToCsv();

        ParseCSV parseCSV = new ParseCSV();
        parseCSV.parseData();
        parseCSV.parseCSV();

    }
}
