import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by joy on 7/24/17.
 */
public class HelperPOI {


    public void convertToCsv() throws IOException, InvalidFormatException {
        //String inputFilePath = "/home/joy/Desktop/text.xlsx";

        File csvFile = new File("output.csv");

        //OPCPackage pkg = OPCPackage.open(new File(inputFilePath));

       Workbook wb = new XSSFWorkbook(new File("/home/joy/Desktop/text.xlsx"));

        DataFormatter formatter = new DataFormatter();
        PrintStream out = new PrintStream(new FileOutputStream(csvFile),true, "UTF-8");
        for (Sheet sheet : wb) {

            for (Row row : sheet) {

                boolean firstCell = true;
                for (Cell cell : row) {
                    if ( ! firstCell )
                        out.print(',');

                    String text = formatter.formatCellValue(cell);
                    out.print(text);
                    firstCell = false;
                }
                out.println();
            }
        }
    }
}
